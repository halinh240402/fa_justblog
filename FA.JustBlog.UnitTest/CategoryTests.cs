﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.UnitTest
{
    public class CategoryTests
    {
        private CategoryRepository _respository;
        private readonly JustBlogContext _context = new();

        [SetUp]
        public void Setup()
        {
            _respository = new CategoryRepository();
        }

        [Test]
        public void FindCateByIdSuccess() {
            Category c = new Category { 
                Id = 1,
                Name = "Test",
                Description = "Test"
            };
            Assert.AreEqual("Technology", _respository.Find(c.Id).Name);
        }

        [Test]
        public void FindCateByIdNotFound()
        {
            Category c = new Category
            {
                Id = 1111,
                Name = "Test",
                Description = "Test"
            };
            Assert.AreEqual(null, _respository.Find(c.Id));
        }

        [Test]
        public void AddCategorySuccess()
        {
            Category category = new Category()
            {
                Name = "Test",
                UrlSlug = "Test",
                Description = "Test",
            };
            Assert.DoesNotThrow(() => _respository.AddCategory(category));
        }

        [Test]
        public void AddCategoryWithoutName()
        {
            Category category = new Category()
            {
                UrlSlug = "Test",
                Description = "Test",
            };
            Assert.Throws<Exception>(() => _respository.AddCategory(category));
        }

        [Test]
        public void AddCategoryWithNameWithSpecialCharacters()
        {
            Category category = new Category()
            {
                UrlSlug = "ff@#",
                Description = "Test",
            };
            Assert.Throws<Exception>(() => _respository.AddCategory(category));
        }

        [Test]
        public void AddCategoryWithNameExceedingMaxLength()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 110)
            {
                stringBuilder.Append("1");
                maxLength++;
            }
            string name = stringBuilder.ToString();
            Category category = new Category()
            {
                Name = name,
                UrlSlug = "Test",
                Description = "Test",
            };
            Assert.Throws<Exception>(() => _respository.AddCategory(category));
        }

        [Test]
        public void DeleteCategoryByIdSuccess()
        {
            Category category = new Category() { 
                Id = 1
            };
            Assert.DoesNotThrow(() => _respository.DeleteCategory(category.Id));
        }

        [Test]
        public void DeleteCategoryByIdFail()
        {
            Category category = new Category()
            {
                Id = 1111
            };
            Assert.Throws<Exception>(() => _respository.DeleteCategory(category.Id));
        }

        [Test]
        public void GetAllCates()
        {
            int count = _respository.GetAllCategories().Count;
            Assert.AreEqual(4, count);
        }

    }
}
