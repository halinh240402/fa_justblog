﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.UnitTest
{
    public class TagTests
    {
        private TagRepository repo;

        [SetUp]
        public void Test()
        {
            repo = new TagRepository();
        }

        [Test]
        public void GetAllTags()
        {
            int count = repo.GetAllTags().Count;
            Assert.AreEqual(5, count);
        }

        [Test]
        public void FindTagByIdSuccess()
        {
            Tag tag = repo.Find(1);
            Assert.IsNotNull(tag);
        }

        [Test]
        public void FindTagByIdNotFound()
        {
            Tag tag = repo.Find(111);
            Assert.IsNull(tag);
        }

        [Test]
        public void AddTagSuccess()
        {
            Tag t = new Tag()
            {
                Name = "Test tag",
                UrlSlug = "test tag",
                Description = "description test tag",
                Count = 1
            };
            Assert.DoesNotThrow(() => repo.AddTag(t));
        }

        [Test]
        public void AddTagWithoutName()
        {
            Tag t = new Tag()
            {
                Description = "Test",
                UrlSlug = "Test",
            };
            Assert.Throws<Exception>(() => repo.AddTag(t));
        }

        [Test]
        public void AddTagWithNameExceeding()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 110)
            {
                stringBuilder.Append("1");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Tag tag = new Tag()
            {
                Name = testString,
                UrlSlug = "test",
                Description = "test",
                Count = 1
            };
            Assert.Throws<Exception>(() => repo.AddTag(tag));
        }

        [Test]
        public void AddTagWithNameWithSpecialCharacters()
        {
            Tag tag = new Tag()
            {
                Name = "test-name@",
                UrlSlug = "test",
                Description = "test",
                Count = 1
            };
            Assert.Throws<Exception>(() => repo.AddTag(tag));
        }

    }
}
