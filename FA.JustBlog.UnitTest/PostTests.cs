﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.UnitTest
{
    public class PostTests
    {
        private PostRepository repo;
        private readonly JustBlogContext context = new();

        [SetUp]
        public void Setup()
        {
            repo = new PostRepository();
        }

        [Test]
        public void FindPostByIdSuccess()
        {
            Post c = new Post
            {
                Id = 3,
                Title = "C# 1",
            };
            Assert.AreEqual("Learning Entity Framework", repo.FindPostById(c.Id).Title);
        }

        [Test]
        public void FindPostByIdNotFound()
        {
            Post c = new Post
            {
                Id = 111,
                Title = "C# 1",
            };
            Assert.AreEqual(null, repo.FindPostById(c.Id));
        }

        [Test]
        public void DeletePostSuccess()
        {
            Post c = new Post
            {
                Id = 2
            };
            Assert.DoesNotThrow(() => repo.DeletePost(c.Id));
        }

        [Test]
        public void DeletePostNotFound()
        {
            Post c = new Post
            {
                Id = 111
            };
            Assert.Throws<Exception>(() => repo.DeletePost(c.Id));
        }

        [Test]
        public void GetAllPosts()
        {
            int count = repo.GetAllPosts().Count();
            Assert.AreEqual(1, count);
        }

        [Test]
        public void AddPostSuccess()
        {
            Post p = new Post {
                Title = "Test",
                ShortDescription = "Test",
                UrlSlug = "test",
                PostContent = "Test",
                PostedOn = DateTime.Now,
                Published = true,
                CategoryId = 2,
            };
            Assert.DoesNotThrow(() => repo.AddPost(p));
        }

        [Test]
        public void AddPostWithoutTitle()
        {
            Post p = new Post
            {
                ShortDescription = "Test",
                PostContent = "Test",
                PostedOn = DateTime.Now,
                Published = true,
                CategoryId = 1,
            };
            Assert.Throws<Exception>(() => repo.AddPost(p));
        }

        [Test]
        public void AddPostWithTitleExceeding()
        {
            StringBuilder stringBuilder = new StringBuilder();
            int maxLength = 0;
            while (maxLength < 110)
            {
                stringBuilder.Append("1");
                maxLength++;
            }
            string testString = stringBuilder.ToString();
            Post tag = new Post()
            {
                Title = testString,
                UrlSlug = "test"
            };
            Assert.Throws<Exception>(() => repo.AddPost(tag));
        }

        [Test]
        public void AddPostWithTitleWithSpecialCharacters()
        {
            Post tag = new Post()
            {
                Title = "Test!@#",
                UrlSlug = "test"
            };
            Assert.Throws<Exception>(() => repo.AddPost(tag));
        }
    }

    
}
