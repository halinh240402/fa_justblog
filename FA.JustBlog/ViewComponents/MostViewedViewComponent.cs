﻿using FA.JustBlog.Core.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.ViewComponents
{
    public class MostViewedViewComponent : ViewComponent
    {
        IPostRepo repo;

        public MostViewedViewComponent(IPostRepo repo)
        {
            this.repo = repo;
        }

        public IViewComponentResult Invoke()
        {
            int postCount = 5;
            var mostViewed = repo.GetMostViewedPost(postCount);
            return View(mostViewed);
        }
    }
}
