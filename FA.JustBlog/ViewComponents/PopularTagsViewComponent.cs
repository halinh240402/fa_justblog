﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.ViewComponents
{
    public class PopularTagsViewComponent : ViewComponent
    {
        ITagRepo tagRepo;
        public PopularTagsViewComponent(ITagRepo tagRepo)
        {
            this.tagRepo = tagRepo;
        }   

        public IViewComponentResult Invoke()
        {
            int size = 10;
            IList<Tag> tags = tagRepo.GetMostPopularTags(size);
            return View();
        }
    }
}
