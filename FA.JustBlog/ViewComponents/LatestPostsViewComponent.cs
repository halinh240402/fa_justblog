﻿using FA.JustBlog.Core.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.ViewComponents
{
    public class LatestPostsViewComponent : ViewComponent
    {
        IPostRepo repo;

        public LatestPostsViewComponent(IPostRepo repo)
        {
            this.repo = repo;
        }

        public IViewComponentResult Invoke()
        {
            int postCount = 5;
            var lastests = repo.GetLatestPost(postCount);
            return View(lastests);
        }
    }
}
