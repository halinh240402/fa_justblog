﻿using FA.JustBlog.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
namespace FA.JustBlog.ViewComponents
{
    public class MenuViewComponent : ViewComponent
    {
        private readonly ICategoryRepo _categoryRepository;

        public MenuViewComponent(ICategoryRepo categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }
        public IViewComponentResult Invoke()
        {
            var categories = _categoryRepository.GetAllCategories();
            return View(categories);
        }

    }
}
