﻿using FA.JustBlog.Areas.Admin.Models;
using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repositories;
using FA.JustBlog.Core.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class TagController : Controller
    {
        private readonly ITagRepo _tagRepository;

        public TagController(ITagRepo tagRepo)
        {
            this._tagRepository = tagRepo;
        }
       
        public IActionResult Index()
        {
            var tags = _tagRepository.GetAllTags();
            return View(tags);
        }

        public IActionResult Details(int id)
        {
            var tag = _tagRepository.Find(id);
            if (tag == null)
            {
                return NotFound();
            }
            return View(tag);
        }

        [HttpGet]
        [Authorize(Roles = $"{Roles.BlogOwner}, {Roles.Contributor}")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = $"{Roles.BlogOwner}, {Roles.Contributor}")]
        public IActionResult Create(TagViewModel model)
        {
            if (ModelState.IsValid)
            {
                var tag = MapToTag(model);
                _tagRepository.AddTag(tag);
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpGet]
        [Authorize(Roles = $"{Roles.BlogOwner}, {Roles.Contributor}")]
        public IActionResult Edit(int id)
        {
            var tag = _tagRepository.Find(id);
            if (tag == null)
            {
                return NotFound();
            }
            var tagModel = MapToTagViewModel(tag);
            return View(tagModel);
        }
        [HttpPost]
        [Authorize(Roles = $"{Roles.BlogOwner}, {Roles.Contributor}")]
        public IActionResult Edit(TagViewModel model)
        {
            if (ModelState.IsValid)
            {
                var tag = MapToTag(model);
                _tagRepository.UpdateTag(tag);
                return RedirectToAction("Index");
            }
            return View();
        }

        [HttpGet]
        [Authorize(Roles = $"{Roles.BlogOwner}")]
        public IActionResult Delete(int id)
        {
            var tag = _tagRepository.Find(id);
            if (tag == null)
            {
                return NotFound();
            }
            return View(tag);
        }

        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = $"{Roles.BlogOwner}")]
        public IActionResult DeleteConfirmed(int id)
        {
            var tag = _tagRepository.Find(id);
            if (tag == null)
            {
                return NotFound();
            }
            _tagRepository.DeleteTag(tag);
            return RedirectToAction("Index");
        }

        private Tag MapToTag(TagViewModel model) {
            Tag tag = new()
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                UrlSlug = model.UrlSlug
            };
            return tag;
        }

        private TagViewModel MapToTagViewModel(Tag tag) { 
            TagViewModel model = new TagViewModel()
            {
                Id = tag.Id,
                Name = tag.Name,
                Description = tag.Description,
                UrlSlug = tag.UrlSlug
            };
            return model;
        }
    }
}
