﻿using FA.JustBlog.Areas.Admin.Models;
using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repositories;
using FA.JustBlog.Core.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CategoryController : Controller
    {
        private readonly ICategoryRepo _categoryRepository;
        public CategoryController(ICategoryRepo categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }
        public IActionResult Index()
        {
            var categories = _categoryRepository.GetAllCategories();
            return View(categories);
        }
        
        public IActionResult Details(int id)
        {
            var category = _categoryRepository.Find(id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category);
        }

        [HttpGet]
        [Authorize(Roles = $"{Roles.BlogOwner}, {Roles.Contributor}")]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [Authorize(Roles = $"{Roles.BlogOwner}, {Roles.Contributor}")]
        public IActionResult Create(CategoryViewModel categoryView)
        {
            if (ModelState.IsValid)
            {
                var category = MapToCategory(categoryView);
                _categoryRepository.AddCategory(category);
                return RedirectToAction("Index");
            }
            return View();
        }

        
        [HttpGet]
        [Authorize(Roles = $"{Roles.BlogOwner}, {Roles.Contributor}")]
        public IActionResult Edit(int id)
        {
            var category = _categoryRepository.Find(id);
            if (category == null)
            {
                return NotFound();
            }
            var categoryModel = MapToViewModel(category);
            return View(categoryModel);
        }
        [HttpPost]
        [Authorize(Roles = $"{Roles.BlogOwner}, {Roles.Contributor}")]
        public IActionResult Edit(CategoryViewModel categoryViewModel)
        {
            if (ModelState.IsValid)
            {
                var category = MapToCategory(categoryViewModel);
                _categoryRepository.UpdateCategory(category);
                return RedirectToAction("Index");
            }
            return View();
        }

        private Category MapToCategory(CategoryViewModel model)
        {
            Category category = new()
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                UrlSlug = model.UrlSlug,
            };
            return category;
        }

        private CategoryViewModel MapToViewModel(Category category) {
            CategoryViewModel model = new()
            {
                Id = category.Id,
                Name = category.Name,
                Description = category.Description,
                UrlSlug = category.UrlSlug
            };
            return model;
        }
    }
}
