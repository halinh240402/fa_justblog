﻿using FA.JustBlog.Areas.Admin.Models;
using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repositories;
using FA.JustBlog.Core.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class PostController : Controller
    {
        private readonly IPostRepo postRepo;
        private readonly ICategoryRepo categoryRepo;
        public PostController(IPostRepo postrepo, ICategoryRepo cate)
        {
            postRepo = postrepo;
            categoryRepo = cate;
        }
        public IActionResult Index()
        {
            IList<Post> list = postRepo.GetAllPosts();
            return View(list);
        }

        public IActionResult LatestPosts(int size = 15)
        {
            IList<Post> list = postRepo.GetLatestPost(size);
            return View(list);
        }

        public IActionResult MostViewedPosts(int size = 15)
        {
            IList<Post> list = postRepo.GetMostViewedPost(size);
            return View(list);
        }

        public IActionResult MostInterestingPosts()
        {
            var posts = postRepo.GetAllPosts().OrderByDescending(p => p.TotalRate).ToList();
            return View(posts);
        }

        public IActionResult PublishedPosts()
        {
            IList<Post> list = postRepo.GetPublisedPosts();
            return View(list);
        }

        [Authorize(Roles = Roles.BlogOwner)]
        public IActionResult UnPublishedPosts()
        {
            IList<Post> list = postRepo.GetUnpublisedPosts();
            return View(list);
        }

        [HttpGet]
        [Authorize(Roles = $"{Roles.BlogOwner}, {Roles.Contributor}")]
        public IActionResult Create()
        {
            var categories = categoryRepo.GetAllCategories();
            ViewBag.Categories = categories;
            return View();
        }

        [HttpPost]
        [Authorize(Roles = $"{Roles.BlogOwner}, {Roles.Contributor}")]
        public IActionResult Create(PostDto model)
        {
            var categories = categoryRepo.GetAllCategories();
            ViewBag.Categories = categories;
            if (ModelState.IsValid)
            {
                Post postToCreate = mapToPost(model);
                postToCreate.Modified = model.PostedOn;
                postRepo.AddPost(postToCreate);
                return RedirectToAction("Index");
            }
            return View();
        }

        public IActionResult Details(int id)
        {
            Post post = postRepo.FindPostById(id);
            if (post == null)
            {
                return NotFound();
            }
            return View(post);
        }

        [HttpGet]
        [Authorize(Roles = $"{Roles.BlogOwner}, {Roles.Contributor}")]
        public IActionResult Edit(int id)
        {
            var categories = categoryRepo.GetAllCategories();
            ViewBag.Categories = categories;
            Post post = postRepo.FindPostById(id);
            if (post == null)
            {
                return NotFound();
            }
            PostDto dto = mapperToDto(post);
            return View(dto);
        }

        [HttpPost]
        [Authorize(Roles = $"{Roles.BlogOwner}, {Roles.Contributor}")]
        public IActionResult Edit(int id, PostDto dto)
        {
            if (ModelState.IsValid)
            {
                var post = mapToPost(dto);
                post.Modified = DateTime.Now;
                postRepo.UpdatePost(post);
                return RedirectToAction("Index");
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Authorize(Roles = Roles.BlogOwner)]
        public IActionResult Delete(int id)
        {
            var post = postRepo.FindPostById(id);
            if (post == null)
            {
                return NotFound();
            }
            return View(post);
        }

        [HttpPost]
        [Authorize(Roles = $"{Roles.BlogOwner}")]
        public IActionResult ConfirmDelete(int id)
        {
            Post postToDelete = postRepo.FindPostById(id);
            if (postToDelete == null)
            {
                return NoContent();
            }
            postRepo.DeletePost(postToDelete);
            return RedirectToAction("Index");
        }

        private Post mapToPost(PostDto model)
        {
            Post post = new()
            {
                Id = model.Id,
                Title = model.Title,
                UrlSlug = model.UrlSlug,
                ShortDescription = model.ShortDescription,
                PostContent = model.PostContent,
                Published = model.Published,
                PostedOn = model.PostedOn,
                Modified = model.Modified,
                CategoryId = model.CategoryId
            };
            return post;
        }

        private PostDto mapperToDto(Post post)
        {
            PostDto dto = new()
            {
                Id = post.Id,
                Title = post.Title,
                UrlSlug = post.UrlSlug,
                ShortDescription = post.ShortDescription,
                PostContent = post.PostContent,
                Published = post.Published,
                PostedOn = post.PostedOn,
                Modified = post.Modified,
                CategoryId = post.CategoryId
            };
            return dto;
        }

    }
}
