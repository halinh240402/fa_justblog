﻿using System.ComponentModel.DataAnnotations;

namespace FA.JustBlog.Areas.Admin.Models
{
    public class CategoryViewModel
    {
        public int Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string UrlSlug { get; set; }
        [StringLength(200)]
        public string Description { get; set; }
    }
}
