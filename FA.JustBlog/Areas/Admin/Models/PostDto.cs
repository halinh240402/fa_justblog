﻿using System.ComponentModel.DataAnnotations;

namespace FA.JustBlog.Areas.Admin.Models
{
    public class PostDto
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Post title is required.")]
        [StringLength(255)]
        public string Title { get; set; }
        [StringLength(100)]
        public string ShortDescription { get; set; }
        [StringLength(500)]
        public string PostContent { get; set; }
        [StringLength(50)]
        public string UrlSlug { get; set; }
        public bool Published { get; set; } = false;
        public DateTime PostedOn { get; set; }
        public DateTime? Modified { get; set; }
        public int CategoryId { get; set; }

    }
}
