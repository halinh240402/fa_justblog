﻿using FA.JustBlog.Core.Models;
using FA.JustBlog.Core.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.Controllers
{
    public class PostController : Controller
    {
        private readonly IPostRepo postRepo;
        public PostController(IPostRepo repo)
        {
            postRepo = repo;
        }

        //display all posts
        public IActionResult Index()
        {
            IList<Post> posts = postRepo.GetAllPosts();
            return View(posts);
        }

        //display single post by id
        public IActionResult PostDetails(int id)
        {
            Post post = postRepo.FindPostById(id);
            if (post == null)
            {
                return NoContent();
            }
            return View("Views/Post/Details.cshtml", post);
        }

        //display latest posts
        public IActionResult LastestPosts(int size = 5)
        {
            IList<Post> posts = postRepo.GetLatestPost(size);
            return View("Views/Post/Index.cshtml", posts);
        }

        //[System.Web.Mvc.ChildActionOnly]
        public IActionResult AboutCard()
        {
            return PartialView("_PartialAboutCard");
        }

        public Post GetPostById(int id)
        {
            return postRepo.FindPostById(id);
        }

        [Route("post/{year}/{month}/{urlslug}")]
        public ActionResult Details(int year, int month, string urlslug)
        {
            var post = postRepo.FindPost(year, month, urlslug);
            if (post == null)
                return NotFound();
            return View(post);
        }

        // get post by category name
        [Route("post/category/{category}")]
        public IActionResult Category(string category)
        {
            try
            {
                IList<Post> postsByCate = postRepo.GetPostsByCategory(category);
                return View("Views/Post/Index.cshtml", postsByCate);
            }
            catch (Exception ex)
            {
                return RedirectToAction(nameof(ReturnErrorView));
            }
        }

        // get posts by tag name
        [Route("post/tag/{tagUrlSlug}")]
        public IActionResult Tag(string tagUrlSlug)
        {
            try
            {
                IList<Post> postsByTag = postRepo.GetPostsByTag(tagUrlSlug);
                return View("Views/Post/Index.cshtml", postsByTag);
            }
            catch (Exception ex)
            {
                return RedirectToAction(nameof(ReturnErrorView));
            }
        }

        [HttpPost]
        //[Route("post/search")]
        public IActionResult Search(string query)
        {
            IList<Post> posts = postRepo.FindPostByTitleOrDescription(query);
            return View("Views/Post/Index.cshtml", posts);
        }
        public ViewResult ReturnErrorView()
        {
            return View("Views/Shared/Error.cshtml");
        }
    }
}
