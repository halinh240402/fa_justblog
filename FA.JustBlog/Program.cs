using FA.JustBlog.Core.Repositories;
using FA.JustBlog.Core.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using FA.JustBlog.Core.Utilities;
using Microsoft.AspNetCore.Identity.UI.Services;

namespace FA.JustBlog
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            var connectionString = builder.Configuration.GetConnectionString("JustBlogContextConnection") ?? throw new InvalidOperationException("Connection string 'JustBlogContextConnection' not found.");

            builder.Services.AddDbContext<JustBlogContext>(options => options.UseSqlServer(connectionString));
            builder.Services.AddIdentity<IdentityUser, IdentityRole>()
                .AddEntityFrameworkStores<JustBlogContext>()
                .AddDefaultTokenProviders();

            builder.Services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = $"/Identity/Account/Login";
                options.LogoutPath = $"/Identity/Account/Logout";
                options.AccessDeniedPath = $"/Identity/Account/AccessDenied";
            });

            builder.Services.AddRazorPages();

            // Add services to the container.
            builder.Services.AddControllersWithViews();
            builder.Services.AddScoped<IPostRepo, PostRepository>();
            builder.Services.AddScoped<ICategoryRepo, CategoryRepository>();
            builder.Services.AddScoped<ITagRepo, TagRepository>();
            builder.Services.AddScoped<IEmailSender, EmailSender>();
            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (!app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.MapRazorPages();
            app.MapControllerRoute(
                name: "Admin",
                pattern: "{area:exists}/{controller=Post }/{action=Index}/{id?}");

            app.MapControllerRoute(
                name: "default",
                pattern: "{controller=Post}/{action=Index}");

            app.MapControllerRoute(
                name: "LastestPost",
                pattern: "{controller=Post}/{action=LastestPosts}/{size?}");

            app.MapControllerRoute(
                name: "Post",
                pattern: "Post/{year}/{month}/{urlslug}",
                defaults: new { controller = "Post", action = "Details" }
            );

            app.MapControllerRoute(
                name: "Category",
                pattern: "Post/Category/{name}",
                defaults: new { controller = "Post", action = "Category" }
            );

            app.MapControllerRoute(
                name: "Tag",
                pattern: "Post/Tag/{name}",
                defaults: new { controller = "Post", action = "Tag" }
            );

            app.Run();
        }
    }
}
