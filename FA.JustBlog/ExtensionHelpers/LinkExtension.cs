﻿using FA.JustBlog.Core.Models;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Globalization;

namespace FA.JustBlog.ExtensionHelpers
{
    public static class LinkExtension
    {
        public static IHtmlContent CategoryLink(this IHtmlHelper helper, string categoryName, string urlSlug)
        {
            var url = $"/Post/Category/{urlSlug}";
            var anchor = new TagBuilder("a");
            anchor.Attributes["href"] = url;
            anchor.InnerHtml.Append(categoryName);

            using (var writer = new StringWriter())
            {
                anchor.WriteTo(writer, System.Text.Encodings.Web.HtmlEncoder.Default);
                return new HtmlString(writer.ToString());
            }
        }

        public static IHtmlContent TagLink(this IHtmlHelper helper, string tagName, string urlSlug)
        {
            var url = $"/Post/Tag/{urlSlug}";
            var anchor = new TagBuilder("a");
            anchor.Attributes["href"] = url;
            anchor.InnerHtml.Append(tagName);

            using (var writer = new StringWriter())
            {
                anchor.WriteTo(writer, System.Text.Encodings.Web.HtmlEncoder.Default);
                return new HtmlString(writer.ToString());
            }
        }

        public static IHtmlContent PostedTime(this IHtmlHelper helper, Post post)
        {
            var timeSpan = DateTime.Now - post.PostedOn;
            string friendlyTime;

            if (timeSpan.TotalMinutes < 60)
                friendlyTime = $"{timeSpan.TotalMinutes:F0} minutes ago";
            else if (timeSpan.TotalHours < 24)
                friendlyTime = $"{timeSpan.TotalHours:F0} hours ago";
            else if (timeSpan.TotalDays < 2)
            {
                friendlyTime = $"yesterday at {post.PostedOn.ToShortTimeString()}";
            }
            else
                friendlyTime = post.PostedOn.ToString("dd-MM-yyyy 'at' hh:mm tt", CultureInfo.InvariantCulture);

            string displayText = $"Posted {friendlyTime} Viewed {post.ViewCount} times with Rate {post.RateCount}";
            var displayTag = new TagBuilder("p");
            displayTag.AddCssClass("post-meta");
            displayTag.InnerHtml.Append(displayText);

            using (var writer = new StringWriter())
            {
                displayTag.WriteTo(writer, System.Text.Encodings.Web.HtmlEncoder.Default);
                return new HtmlString(writer.ToString());
            }
        }
    }
}
