﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Models
{
    public class JustBlogContext : IdentityDbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<PostTagMap> PostTagMaps { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }

        public JustBlogContext() { }

        public JustBlogContext(DbContextOptions<JustBlogContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("server=Halinh\\SQLEXPRESS;database=FA_JustBlog_3;TrustServerCertificate=true;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Category>(entity =>
            {
                entity.HasKey(c => c.Id);

                entity.Property(c => c.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(c => c.UrlSlug)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(c => c.Description)
                    .HasMaxLength(500);
            });

            // Configure Tag entity
            modelBuilder.Entity<Tag>(entity =>
            {
                entity.HasKey(t => t.Id);

                entity.Property(t => t.Name)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(t => t.UrlSlug)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(t => t.Description)
                    .HasMaxLength(500);

                entity.Property(t => t.Count);
            });

            // Configure Post entity
            modelBuilder.Entity<Post>(entity =>
            {
                entity.HasKey(p => p.Id);

                entity.Property(p => p.Title)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(p => p.ShortDescription)
                    .HasMaxLength(500);

                entity.Property(p => p.PostContent)
                    .HasColumnType("ntext");

                entity.Property(p => p.UrlSlug)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(p => p.Published);

                entity.Property(p => p.PostedOn);

                entity.Property(p => p.Modified).IsRequired(false);

                entity.HasOne(p => p.Category)
                    .WithMany(c => c.Posts)
                    .HasForeignKey(p => p.CategoryId);

            });

            // Configure PostTagMap entity
            modelBuilder.Entity<PostTagMap>(entity =>
            {
                entity.HasKey(pt => new { pt.PostId, pt.TagId });

                entity.HasOne<Post>(e => e.Post)
                    .WithMany(p => p.PostTagMaps)
                    .HasForeignKey(pt => pt.PostId);

                entity.HasOne<Tag>(p => p.Tag)
                    .WithMany(t => t.PostTagMaps)
                    .HasForeignKey(pt => pt.TagId);
            });

            // config comment
            modelBuilder.Entity<Comment>(entity =>
            {
                entity.HasKey(c => c.Id);

                entity.Property(c => c.Name)
                      .IsRequired()
                      .HasMaxLength(100);

                entity.Property(c => c.Email)
                      .IsRequired()
                      .HasMaxLength(50);

                entity.Property(c => c.CommentHeader).HasMaxLength(50);

                entity.Property(c => c.CommentText)
                      .IsRequired()
                      .HasMaxLength(100);

                entity.Property(c => c.CommentTime);

                entity.HasOne(c => c.Post)
                    .WithMany(p => p.Comments)
                    .HasForeignKey(p => p.PostId);
            });
            DbInitializer.Initilizer(modelBuilder);
        }
    }
}
