﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Models
{
    public class DbInitializer
    {
        public static void Initilizer(ModelBuilder modelBuilder)
        {
            var categories = new List<Category>
        {
            new Category {Id=1, Name = "Technology", UrlSlug = "technology", Description = "All about technology" },
            new Category {Id=2,  Name = "Lifestyle", UrlSlug = "lifestyle", Description = "All about lifestyle" },
            new Category {Id=3,  Name = "Education", UrlSlug = "education", Description = "All about education" }
        };
            modelBuilder.Entity<Category>().HasData(categories);

            var tags = new List<Tag>
        {
            new Tag {Id=1, Name = "C#", UrlSlug = "c-sharp", Description = "C# programming language", Count = 5 },
            new Tag {Id=2, Name = "ASP.NET", UrlSlug = "asp-net", Description = "ASP.NET framework", Count = 5 },
            new Tag {Id=3, Name = "Entity Framework", UrlSlug = "entity-framework", Description = "Entity Framework ORM", Count = 3 }
        };
            modelBuilder.Entity<Tag>().HasData(tags);

            var posts = new List<Post>
        {
            new Post
            {
                Id = 1,
                Title = "C# 1",
                ShortDescription = "C# intro 1",
                PostContent = "C# introduction post",
                UrlSlug = "introduction-to-csharp",
                Published = true,
                PostedOn = DateTime.Now,
                Modified = DateTime.Now,
                CategoryId = 1
            },
            new Post
            {
                Id = 2,
                Title = "EF Core",
                ShortDescription = "Tips for EF",
                PostContent = "Content of EF",
                UrlSlug = "ef-tip",
                Published = true,
                PostedOn = DateTime.Now,
                Modified = DateTime.Now,
                CategoryId = 2
            },
            new Post
            {
                Id=3,
                Title = "Learning Entity Framework",
                ShortDescription = "Guide to learning Entity Framework",
                PostContent = "Content of the Entity Framework post",
                UrlSlug = "learning-entity-framework",
                Published = true,
                PostedOn = DateTime.Now,
                Modified = DateTime.Now,
                CategoryId = 3
            }
        };
            modelBuilder.Entity<Post>().HasData(posts);

            var postTagMaps = new List<PostTagMap>
            {
                new PostTagMap { PostId = 1, TagId = 1 },
                new PostTagMap { PostId = 1, TagId = 2 },
                new PostTagMap { PostId = 2, TagId = 2 }
            };
            modelBuilder.Entity<PostTagMap>().HasData(postTagMaps);

            var comments = new List<Comment>
            {
                new Comment{
                    Id = 1,
                    Name = "User 1",
                    Email="user1@gmail.com",
                    PostId = 3,
                    CommentText = "Comment 1",
                    CommentTime = DateTime.Now,
                    CommentHeader = "Header"
                },
                new Comment{
                    Id = 2,
                    Name = "User 2",
                    Email="user2@gmail.com",
                    PostId = 3,
                    CommentText = "Comment 2",
                    CommentTime = DateTime.Now,
                    CommentHeader = "Header 2"
                },
                new Comment{
                    Id = 3,
                    Name = "User 1",
                    Email="user1@gmail.com",
                    PostId = 3,
                    CommentText = "Comment 3",
                    CommentTime = DateTime.Now,
                    CommentHeader = "Header 3"
                },
            };
            modelBuilder.Entity<Comment>().HasData(comments);
        }
    }
}
