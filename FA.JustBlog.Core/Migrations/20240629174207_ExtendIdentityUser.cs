﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FA.JustBlog.Core.Migrations
{
    /// <inheritdoc />
    public partial class ExtendIdentityUser : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AboutMe",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Age",
                table: "AspNetUsers",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetUsers",
                type: "nvarchar(21)",
                maxLength: 21,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "AspNetUsers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Comment",
                keyColumn: "Id",
                keyValue: 1,
                column: "CommentTime",
                value: new DateTime(2024, 6, 30, 0, 42, 5, 863, DateTimeKind.Local).AddTicks(1586));

            migrationBuilder.UpdateData(
                table: "Comment",
                keyColumn: "Id",
                keyValue: 2,
                column: "CommentTime",
                value: new DateTime(2024, 6, 30, 0, 42, 5, 863, DateTimeKind.Local).AddTicks(1593));

            migrationBuilder.UpdateData(
                table: "Comment",
                keyColumn: "Id",
                keyValue: 3,
                column: "CommentTime",
                value: new DateTime(2024, 6, 30, 0, 42, 5, 863, DateTimeKind.Local).AddTicks(1595));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Modified", "PostedOn" },
                values: new object[] { new DateTime(2024, 6, 30, 0, 42, 5, 863, DateTimeKind.Local).AddTicks(1511), new DateTime(2024, 6, 30, 0, 42, 5, 863, DateTimeKind.Local).AddTicks(1500) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Modified", "PostedOn" },
                values: new object[] { new DateTime(2024, 6, 30, 0, 42, 5, 863, DateTimeKind.Local).AddTicks(1521), new DateTime(2024, 6, 30, 0, 42, 5, 863, DateTimeKind.Local).AddTicks(1520) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Modified", "PostedOn" },
                values: new object[] { new DateTime(2024, 6, 30, 0, 42, 5, 863, DateTimeKind.Local).AddTicks(1524), new DateTime(2024, 6, 30, 0, 42, 5, 863, DateTimeKind.Local).AddTicks(1523) });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AboutMe",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Age",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "Comment",
                keyColumn: "Id",
                keyValue: 1,
                column: "CommentTime",
                value: new DateTime(2024, 6, 30, 0, 34, 33, 802, DateTimeKind.Local).AddTicks(7098));

            migrationBuilder.UpdateData(
                table: "Comment",
                keyColumn: "Id",
                keyValue: 2,
                column: "CommentTime",
                value: new DateTime(2024, 6, 30, 0, 34, 33, 802, DateTimeKind.Local).AddTicks(7109));

            migrationBuilder.UpdateData(
                table: "Comment",
                keyColumn: "Id",
                keyValue: 3,
                column: "CommentTime",
                value: new DateTime(2024, 6, 30, 0, 34, 33, 802, DateTimeKind.Local).AddTicks(7113));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Modified", "PostedOn" },
                values: new object[] { new DateTime(2024, 6, 30, 0, 34, 33, 802, DateTimeKind.Local).AddTicks(6941), new DateTime(2024, 6, 30, 0, 34, 33, 802, DateTimeKind.Local).AddTicks(6917) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Modified", "PostedOn" },
                values: new object[] { new DateTime(2024, 6, 30, 0, 34, 33, 802, DateTimeKind.Local).AddTicks(6958), new DateTime(2024, 6, 30, 0, 34, 33, 802, DateTimeKind.Local).AddTicks(6956) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Modified", "PostedOn" },
                values: new object[] { new DateTime(2024, 6, 30, 0, 34, 33, 802, DateTimeKind.Local).AddTicks(6963), new DateTime(2024, 6, 30, 0, 34, 33, 802, DateTimeKind.Local).AddTicks(6962) });
        }
    }
}
