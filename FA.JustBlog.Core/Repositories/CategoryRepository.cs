﻿using Azure;
using FA.JustBlog.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Repositories
{
    public class CategoryRepository : ICategoryRepo
    {
        private readonly JustBlogContext db;
        public CategoryRepository()
        {
            db = new JustBlogContext();
        }
        public void AddCategory(Category category)
        {

            string regex = @"^[\d\w\s]+$";
            if(category.Name == null || category.UrlSlug == null)
            {
                throw new Exception();
            }
            if(category.Name.Length > 100 || category.UrlSlug.Length > 200 || category.Description.Length > 500)
            {
                throw new Exception();
            }
            //if (!Util.CheckRegex(category.Name, regex) || !Util.CheckRegex(category.Description, regex))
            //{
            //    throw new Exception();
            //}
            db.Categories.Add(category);
            db.SaveChanges();
        }

        public void DeleteCategory(Category category)
        {
            db.Categories.Remove(category);
            db.SaveChanges();
        }

        public void DeleteCategory(int categoryId)
        {
            Category? category = db.Categories.FirstOrDefault(c => c.Id == categoryId);
            if (category != null)
            {
                db.Categories.Remove(category);
                db.SaveChanges();
            }
            else
            {
                throw new Exception();
            }
        }

        public Category Find(int categoryId)
        {
            return db.Categories.FirstOrDefault(c => c.Id == categoryId);
        }

        public IList<Category> GetAllCategories()
        {
            return db.Categories.ToList();
        }

        public void UpdateCategory(Category category)
        {
            Category? categoryToUpdate = db.Categories.FirstOrDefault(c => c.Id == category.Id);
            if (categoryToUpdate != null)
            {
                categoryToUpdate.Name = category.Name;
                categoryToUpdate.Description = category.Description;
                categoryToUpdate.UrlSlug = category.UrlSlug;
                categoryToUpdate.Posts = category.Posts;
                db.Categories.Update(categoryToUpdate);
                db.SaveChanges();
            }
        }

    }
}
