﻿using FA.JustBlog.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Repositories
{
    public interface ITagRepo
    {
        Tag Find(int TagId);
        Tag FindTagByUrlSlug(string url);
        void AddTag(Tag Tag);
        void UpdateTag(Tag Tag);
        void DeleteTag(Tag Tag);
        void DeleteTag(int TagId);
        IList<Tag> GetAllTags();
        IList<Tag> GetMostPopularTags(int size);
        Tag GetTagByUrlSlug(string urlSlug);

    }
}
