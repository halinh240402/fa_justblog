﻿using Azure;
using FA.JustBlog.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Repositories
{
    public class TagRepository : ITagRepo
    {
        private readonly JustBlogContext db;
        public TagRepository()
        {
            db = new JustBlogContext();
        }

        public void AddTag(Tag tag)
        {
            string regex = @"^[\d\w\s]+$";
            if (tag.Name == null || tag.UrlSlug == null)
            {
                throw new Exception();
            }
            if(tag.Name.Length > 100)
            {
                throw new Exception();
            }
            if (!Util.CheckRegex(tag.Name, regex) || !Util.CheckRegex(tag.Description, regex))
            {
                throw new Exception();
            }
            db.Tags.Add(tag);
            db.SaveChanges();
        }

        public void DeleteTag(Tag Tag)
        {
            Tag? t = db.Tags.FirstOrDefault(t => t.Id == Tag.Id);
            if(t == null)
            {
                throw new Exception();
            }
            db.Tags.Remove(t);
            //db.SaveChanges();
        }

        public void DeleteTag(int TagId)
        {
            Tag? t = db.Tags.FirstOrDefault(t => t.Id == TagId);
            if (t == null)
            {
                throw new Exception();
            }
            db.Tags.Remove(t);
            //db.SaveChanges();
        }

        public Tag Find(int TagId)
        {
            return db.Tags.FirstOrDefault(t => t.Id == TagId);
        }

        public Tag FindTagByUrlSlug(string url)
        {
            return db.Tags.FirstOrDefault(t => t.UrlSlug.ToLower() == url.ToLower());
        }

        public IList<Tag> GetAllTags()
        {
            return db.Tags.ToList();
        }

        public Tag GetTagByUrlSlug(string urlSlug)
        {
            return db.Tags.FirstOrDefault(t => t.UrlSlug == urlSlug); 
        }

        public IList<Tag> GetMostPopularTags(int size)
        {
            var tags = db.Tags
                        .OrderByDescending(post => post.Count)
                        .Take(size)
                        .ToList();
            return tags;
        }

        public void UpdateTag(Tag tag)
        {
            string regex = @"^[\d\w\s]+$";
            try
            {
                var tagUpdate = db.Tags.FirstOrDefault(x => x.Id == tag.Id);
                if (!Util.CheckRegex(tag.Name, regex) || !Util.CheckRegex(tag.UrlSlug, regex) || !Util.CheckRegex(tag.Description, regex))
                {
                    throw new Exception();
                }
                tagUpdate!.Name = tag.Name;
                tagUpdate.Description = tag.Description;
                tagUpdate.UrlSlug = tag.UrlSlug;
                tagUpdate.Description = tag.Description;
                tagUpdate.Count = tag.Count;
                db.Tags.Update(tagUpdate);
                db.SaveChanges();

            }
            catch (Exception)
            {
                throw new Exception();
            }

        }
    }
}
