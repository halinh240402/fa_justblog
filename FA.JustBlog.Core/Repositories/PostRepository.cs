﻿using FA.JustBlog.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Repositories
{
    public class PostRepository : IPostRepo
    {
        private readonly JustBlogContext db;

        public PostRepository()
        {
            db = new JustBlogContext();
        }
        public void AddPost(Post post)
        {
            string regex = @"^[\d\w\s]+$";
            if (post.Title == null || post.UrlSlug == null)
            {
                throw new Exception();
            }
            if (!Util.CheckRegex(post.Title, regex))
            {
                throw new Exception();
            }
            if (post.Title.Length > 100 || post.UrlSlug.Length > 200)
            {
                throw new Exception();
            }
            db.Add(post);
            db.SaveChanges();
        }

        public int CountPostsForCategory(string category)
        {
            int count = db.Posts.Count(p => p.Category.Name == category);
            return count;
        }

        public int CountPostsForTag(string tag)
        {
            return 0;
        }

        public void DeletePost(Post post)
        {
            Post? postToDelete = db.Posts.FirstOrDefault(p => p.Id == post.Id);
            if (postToDelete == null)
            {
                throw new Exception();
            }
            db.Posts.Remove(postToDelete);
            db.SaveChanges();
        }


        public void DeletePost(int postId)
        {
            Post? postToDelete = db.Posts.FirstOrDefault(p => p.Id == postId);
            if (postToDelete == null)
            {
                throw new Exception();
            }
            db.Posts.Remove(postToDelete);
            db.SaveChanges();
        }

        public Post FindPost(int year, int month, string urlSlug)
        {
            var postFind = db.Posts
                .Include(p => p.Category)
                .FirstOrDefault(x => x.PostedOn.Year == year && x.PostedOn.Month == month && x.UrlSlug.ToLower() == urlSlug.ToLower());
            return postFind;
        }

        public Post FindPostByTitle(int year, int month, string title)
        {
            var postFind = db.Posts
                .Include(p => p.Category)
                .FirstOrDefault(x => x.PostedOn.Year == year && x.PostedOn.Month == month && x.Title.ToLower() == title.ToLower());
            return postFind;
        }

        public Post FindPostById(int postId)
        {
            return db.Posts
                .Include(p => p.Category)
                .Include(p => p.PostTagMaps)
                .ThenInclude(ptm => ptm.Tag)
                .FirstOrDefault(p => p.Id == postId);
        }

        public IList<Post> FindPostByTitleOrDescription(string property)
        {
            string keyword = property.ToLower();
            IList<Post> posts = db.Posts
                            .Include(p => p.Category)
                            .Include(p => p.PostTagMaps)
                            .ThenInclude(ptm => ptm.Tag)
                            .Where(p => p.Title.ToLower().Contains(keyword) ||
                                 p.ShortDescription.ToLower().Contains(keyword))
                            .ToList();
            return posts;
        }

        public IList<Post> GetAllPosts()
        {
            var posts = db.Posts
                .Include(p => p.Category)
                .Include(p => p.PostTagMaps)
                .ThenInclude(ptm => ptm.Tag)
                .ToList();
            return posts;
        }

        public IList<Post> GetHighestPosts(int size)
        {
            return db.Posts.Where(p => p.ViewCount == size).ToList();
        }

        public IList<Post> GetLatestPost(int size)
        {
            var result = db.Posts
                .Include(p => p.Category)
                .Include(p => p.PostTagMaps)
                .ThenInclude(ptm => ptm.Tag)
                .OrderByDescending(x => x.PostedOn).Take(size).ToList();
            return result;
        }
        public IList<Post> GetMostViewedPost(int size)
        {
            var lstPostResult = db.Posts
                .Include(p => p.Category)
                .Include(p => p.PostTagMaps)
                .ThenInclude(ptm => ptm.Tag)
                .OrderByDescending(x => x.ViewCount).Take(size).ToList();
            return lstPostResult;

        }

        public IList<Post> GetPostsByCategory(string category)
        {
            Category cate = db.Categories.FirstOrDefault(x => x.Name.Trim().ToLower() == category.Trim().ToLower())!;
            if (cate == null)
            {
                throw new Exception();
            }
            //var result = db.Posts.Where(x => x.CategoryId == cate.Id).ToList();
            var result = db.Posts
                .Include(p => p.Category)
                .Include(p => p.PostTagMaps)
                .ThenInclude(ptm => ptm.Tag)
                .Where(p => p.CategoryId == cate.Id).ToList();
            return result;
        }

        public IList<Post> GetPostsByMonth(DateTime monthYear)
        {
            var posts = db.Posts
                .Include(p => p.Category)
                .Include(p => p.PostTagMaps)
                .ThenInclude(ptm => ptm.Tag)
                .Where(x => x.PostedOn.Month == monthYear.Month).ToList();
            return posts;

        }

        public IList<Post> GetPostsByTag(string tagUrlSlug)
        {
            Tag? tagToFind = db.Tags.FirstOrDefault(t => t.UrlSlug.ToLower() == tagUrlSlug.ToLower());
            if (tagToFind == null)
            {
                throw new ArgumentNullException();
            }
            var posts = db.Posts
                .Include(p => p.Category)
                .Include(p => p.PostTagMaps)
                .ThenInclude(ptm => ptm.Tag)
                .Where(p => p.PostTagMaps.Any(pt => pt.Tag.Id == tagToFind.Id))
                .ToList();
            return posts;
        }

        public IList<Post> GetPublisedPosts()
        {
            return db.Posts
                .Include(p => p.Category)
                .Include(p => p.PostTagMaps)
                .ThenInclude(ptm => ptm.Tag)
                .Where(p => p.Published == true).ToList();
        }

        public IList<Post> GetUnpublisedPosts()
        {
            return db.Posts
                .Include(p => p.Category)
                .Include(p => p.PostTagMaps)
                 .ThenInclude(ptm => ptm.Tag)
                .Where(p => p.Published == false).ToList();
        }

        public void UpdatePost(Post post)
        {
            string regex = @"^[\d\w\s]+$";
            try
            {
                if (!Util.CheckRegex(post.Title, regex))
                {
                    throw new Exception();
                }
                var postUpdate = db.Posts.FirstOrDefault(x => x.Id == post.Id)!;
                postUpdate.Title = post.Title;
                postUpdate.ShortDescription = post.ShortDescription;
                postUpdate.PostContent = post.PostContent;
                postUpdate.UrlSlug = post.UrlSlug;
                postUpdate.Published = post.Published;
                postUpdate.PostedOn = post.PostedOn;
                postUpdate.Modified = post.Modified;
                postUpdate.CategoryId = post.CategoryId;
                db.Posts.Update(postUpdate);
                db.SaveChanges();
            }
            catch (Exception)
            {
                throw new Exception();
            }

        }
    }
}
