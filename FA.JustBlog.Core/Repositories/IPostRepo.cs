﻿using FA.JustBlog.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Repositories
{
    public interface IPostRepo
    {
        Post FindPost(int year, int month, string urlSlug);
        Post FindPostByTitle(int year, int month, string title);
        IList<Post> FindPostByTitleOrDescription(string keyword);
        Post FindPostById(int postId);
        void AddPost(Post post);
        void UpdatePost(Post post);
        void DeletePost(Post post);
        void DeletePost(int postId);
        IList<Post> GetAllPosts();
        IList<Post> GetPublisedPosts();
        IList<Post> GetUnpublisedPosts();
        IList<Post> GetLatestPost(int size);
        IList<Post> GetPostsByMonth(DateTime monthYear);
        int CountPostsForCategory(string category);
        IList<Post> GetPostsByCategory(string category);
        IList<Post> GetPostsByTag(string tagurlSlug);
        IList<Post> GetMostViewedPost(int size);
        IList<Post> GetHighestPosts(int size);

    }
}
