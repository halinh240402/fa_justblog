﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core
{
    public class Util
    {
        public static bool CheckRegex(string input, string regex)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(input, regex);
        }
    }
}
